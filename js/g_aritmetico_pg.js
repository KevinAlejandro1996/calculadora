$(document).ready(function(){
	$("#calcular").click(function(e){
		var interes = parseFloat(document.getElementById('v_interes').value) / 100;
		var tiempo = document.getElementById('v_tiempo').value;
		var gradiente = document.getElementById('v_gradiente').value;
		var resultado;

		if(tiempo && interes && gradiente){
			var funcion_2 = tiempo / Math.pow((1+interes), tiempo);
			var funcion_1 = (Math.pow(1 + parseFloat(interes), tiempo) - 1  ) / (interes * Math.pow(1 + parseFloat(interes) , tiempo));

			resultado = (gradiente / interes) * (parseFloat(funcion_1) - parseFloat(funcion_2));

			console.log(resultado);
		}else{
			alert("Verifique los campos hay al menos uno sin llenar");
		}
	});
});	